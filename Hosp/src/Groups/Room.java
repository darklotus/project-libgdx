/**
 * 
 */
package Groups;

import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;


import Helpers.Logger;
import Objects.GameObject;
import Objects.NPC;
import Objects.Patient;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.AtomicQueue;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.OrderedMap;

/**
 * @author James
 *
 */
public abstract class Room extends Group {
	private int height,width,layers;
	public Array<String> neededObjects = new Array<String>();
	private NPC _currentEmployee;

	public Queue<Patient> _patientQueue = new LinkedList<Patient>();

	
	public Room() {

		//loadMap("room");
		
	}


	public void addPatientToQueue(Patient npc){
		_patientQueue.add(npc);
	}
	
	public Patient getNextPatient(){
		return _patientQueue.poll();
	}
	
	

	public void loadMap(String mapname){
		FileHandle fileHandle = Gdx.files.internal("data/"+mapname+".json");
		OrderedMap root = (OrderedMap) new JsonReader().parse(fileHandle);
		//height = Integer.parseInt(root.get("height")));
		//width = (Integer) root.get("width");
		height = 5; width = 5;
		Array<OrderedMap> layers =  (Array<OrderedMap>) root.get("layers");
		for (int i = 0; i < layers.size; i++) {
			if(layers.get(i)== null)
				continue;
			
			
			Array dataString = (Array) layers.get(i).get("data");
			int x = 0, y = height;
			for (Object string : dataString) {
				float id = (Float) string;
				//Actors.Object tile = new Actors.Object((x * 64 + this.x), (y * 64 + this.y), MathUtils.round(id));
				if(MathUtils.round(id) != 0)
					//addActor(tile);
				x++;
				if(x == 5){
					x = 0; y--;
				}
			}
			
		}
}


	public NPC getCurrentEmployee() {
		return _currentEmployee;
	}


	public void addActor(GameObject _currentBuildObject) {
		addActor(_currentBuildObject.getActor());
		
	}


	
	
}
