/**
 * 
 */
package com.mythiksoftware.hosp.Screens;

import Helpers.GraphicsManager;
import UI.MainMenuUI;

import com.badlogic.gdx.Screen;

/**
 * @author James
 *
 */
public class MainMenuScreen implements Screen {

	MainMenuUI UI;
	public MainMenuScreen() {
		UI = new MainMenuUI();
	}
	@Override
	public void render(float delta) {
		UI.render(delta);
		
	}

	@Override
	public void resize(int width, int height) {
		UI.resize(width, height);
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		GraphicsManager.getManager().toString();
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		UI.dispose();
		
	}

}
