/**
 * 
 */
package com.mythiksoftware.hosp.Screens;

import java.util.Random;

import Events.UIListener;
import Events.UIListener.UIEvent;
import Groups.Room;
import Helpers.Logger;
import Objects.GameObject;
import Objects.GridActor;
import UI.InGameUI;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.mythiksoftware.hosp.GameWorld;

/**
 * @author James
 *
 */
public class InGameScreen implements Screen, InputProcessor, GestureListener {

	private int _lastMouseX,_lastMouseY;
	private GameWorld _gameWorld;
	private InGameUI _UI;
	//private GameState currentState;
	
	
	
	private boolean isBuilding = false;
	public InGameScreen(boolean load){
		this();
		_gameWorld.Load();
	}
	
	public InGameScreen() {
		_UI = new InGameUI();
		this._gameWorld = new GameWorld();
		New();
		InputMultiplexer plexer = new InputMultiplexer(Gdx.input.getInputProcessor());
		GestureDetector gdDetector = new GestureDetector(this);
		plexer.addProcessor(gdDetector);
		plexer.addProcessor(0,this);
		Gdx.input.setInputProcessor(plexer);
		_UI.addListener(new UIListener() {	
			private boolean isBuildingRoom;

			@Override
			public void changed(UIEvent event, Actor actor) {
				Logger.Log("Fired UI in GameScreen");
				switch(event.getType()){
				case Save:
					_gameWorld.Save();
					break;
				case Load:
					New();
					_gameWorld.Load();
					break;
				case NewGame:
					New();
					break;
				case BuildObject:
					isBuilding = true;
					_gameWorld.BeginBuild((GameObject)event.getDataObject());
					break;
				case BuildRoom:
					BeginRoomBuild((Room)event.getDataObject());
					
					break;
				}

			}

			private void BeginRoomBuild(Room dataObject) {
				isBuildingRoom = true;
				_gameWorld.BeginRoomBuild(dataObject);
				
			}

			
		});
}
	private void New() {
		isBuilding = false;
		_gameWorld.New();
	}

	@Override
	public void render(float delta) {
		MouseScroll();
		_gameWorld.render(delta);
		_UI.render(delta);
	}

	

	@Override
	public void resize(int width, int height) {
	_gameWorld.resize(width,height);
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		_gameWorld.dispose();
		_UI.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(isBuilding)
			isBuilding = _gameWorld.FinishBuild(screenX,screenY);
		
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		/*if(wall != null){
			Vector2 vec = _worldStage.screenToStageCoordinates(new Vector2(screenX, screenY));
			wall.setPosition((int)vec.x - (6*64),(int)vec.y - (6*64));
		}*/
		return false;
	}
	
	@Override
	public boolean scrolled(int amount) {
		
		_gameWorld.getCamera().zoom += (float)amount /15;
	
		Logger.Log("zoom: " + _gameWorld.getCamera().zoom);
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	 Vector2 _flingVector2 = new Vector2();
	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		_flingVector2.set(velocityX, velocityY);
		_flingVector2.nor();
		Logger.Log("Fling!" + velocityX + " " + velocityY);
		_gameWorld.getCamera().position.add(_flingVector2.x, _flingVector2.y, 0);
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	private static final float _maxZoom = 0.2f;

	private static final float _minZoom = 5;
	@Override
	public boolean zoom(float initialDistance, float distance) {
		float zoom = 0;
		if(initialDistance > distance && _gameWorld.getCamera().zoom < _minZoom){
			//zoomout
			_gameWorld.getCamera().zoom += 0.01f;
		}
		else if(_gameWorld.getCamera().zoom > _maxZoom){
			//zoom in
			
			_gameWorld.getCamera().zoom -= 0.01f;
			
		}

		Logger.Log("Zoom: " + _gameWorld.getCamera().zoom);
		return false;
		
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		
		return false;
	}

	





	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		_lastMouseX = screenX;
		_lastMouseY = screenY;
		// TODO Auto-generated method stub
		if(isBuilding)
			_gameWorld.UpdateBuilding(screenX,screenY);
	
		return false;
	}

	//Keeps camera moving when mosue isnt moving but is near an edge.
		private void MouseScroll()
		{
			if(_lastMouseX == 0 && _lastMouseY == 0)
				return;
			if(_lastMouseX > Gdx.app.getGraphics().getWidth() * 0.85){
				_gameWorld.getCamera().position.add(5, 0, 0);
			}
			if(_lastMouseX < Gdx.app.getGraphics().getWidth() * 0.15){
				_gameWorld.getCamera().position.add(-5, 0, 0);
			}
			if(_lastMouseY > Gdx.app.getGraphics().getHeight() * 0.80){
				_gameWorld.getCamera().position.add(0,-5,0);
			}
			if(_lastMouseY < Gdx.app.getGraphics().getHeight() * 0.15){
				_gameWorld.getCamera().position.add(0,5,0);
			}
			
		}

}
