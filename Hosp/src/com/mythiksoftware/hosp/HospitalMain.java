package com.mythiksoftware.hosp;



import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.mythiksoftware.hosp.Screens.MainMenuScreen;
public class HospitalMain extends Game {//implements ApplicationListener {
	
public static HospitalMain StaticRefrence;
	private FPSLogger FPS;
//	private InputManager inputManager;
	@Override
	public void create() {		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		FPS = new FPSLogger();
		//inputManager = new InputManager();
		//Gdx.input.setInputProcessor(inputManager);
		setScreen(new MainMenuScreen());
		if(StaticRefrence == null)
			StaticRefrence = this;
	}


	

	@Override
	public void dispose() {
		super.dispose();
		getScreen().dispose();
	}

	@Override
	public void render() {		
		super.render();
		FPS.log();

	}

	@Override
	public void resize(int width, int height) {
		getScreen().resize(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
