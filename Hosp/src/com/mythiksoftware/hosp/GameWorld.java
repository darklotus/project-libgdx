/**
 * 
 */
package com.mythiksoftware.hosp;


import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.omg.CORBA.portable.Streamable;


import Groups.Room;
import Helpers.Logger;
import Objects.GameObject;
import Objects.GridActor;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;


import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Json;



/**
 * @author James
 *
 */
public class GameWorld {	
	public enum GameState{
		BuildMode,
		PlayMode
	}
	
	private GameState _gameState;
	private Stage _worldStage;
	private OrthographicCamera _camera;	
	Level level;
	Map map;
	
	//Transient stuff

	private GameObject _currentBuildObject;
	
	public GameWorld() {
		_worldStage = new Stage();
		
		InputMultiplexer plexer = new InputMultiplexer(Gdx.input.getInputProcessor());
		plexer.addProcessor(_worldStage);
		Gdx.input.setInputProcessor(plexer);
		//Patient patient = new Patient(5,5);
		
		//_worldStage.addActor(patient);
		setCamera((OrthographicCamera) _worldStage.getCamera());
	}

	
	public void render(float delta) {
		
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		if(getGameState() == GameState.PlayMode)
			_worldStage.act(delta);
		map.render();
		_worldStage.draw();

		
		
	}

	public void resize(int width, int height) {
		_worldStage.setViewport(width, height, true);
		
	}

	public void dispose() {
		_worldStage.dispose();
	}

	public OrthographicCamera getCamera() {
		return _camera;
	}


	public void setCamera(OrthographicCamera camera) {
		this._camera = camera;
	}

	
	
	public void BeginBuild(GameObject gameObject) {
			_currentBuildObject = gameObject;
			_currentBuildObject.init();
			if(_currentRoom != null){
				Logger.Log("Attaching actor to Room");
				_currentRoom.addActor(_currentBuildObject);
				}
			else {
				Logger.Log("Attaching actor to Level");
				_worldStage.addActor(_currentBuildObject.getActor());
				//level.addActor(_currentBuildObject);
			}
			
			_currentBuildObject.setTouchability(Touchable.disabled);	
	}


	
	public void UpdateBuilding(int screenX, int screenY) {
		if(_currentBuildObject != null){
			Vector2 vec = _worldStage.screenToStageCoordinates(new Vector2(screenX, screenY));
			map.getCell((int)vec.x, (int)vec.y).setRotation(45);
			GridActor hitActor = (GridActor) _worldStage.hit(vec.x, vec.y, true);
			if(hitActor != null && hitActor.get_owner()instanceof GameObject){
				_currentBuildObject.setColor(Color.RED);
				_canFinishBuild = false;
				return;
			}
			else {
				_canFinishBuild = true;
				_currentBuildObject.setColor(Color.WHITE);
			}
			int x = (int) (vec.x / 64);
			int y = (int) (vec.y /64);
			
			/*if(x < 0 || x > level.Width || y < 0 || y > level.Height){
				_currentBuildObject.setColor(Color.RED);
				_canFinishBuild = false;
				return;
			}
			else*/
				_canFinishBuild = true;
			_currentBuildObject.setPosition(x, y);
		}
		
	}

	boolean _canFinishBuild;
	private String save;
	public boolean FinishBuild(int screenX, int screenY) {
		if(_currentBuildObject != null && _canFinishBuild){
			_currentBuildObject.setTouchability(Touchable.enabled);
			_currentBuildObject = null;
		return false;
		}
		Logger.Log("Invalid Build location, returning true");
		//add logic to return true if object cant be placed/ cant afford now etc.
		return true;
	}


	public void New(){
		map = new Map(getCamera());
		
			if(level != null)
			{
				level.remove();
				level = null;
			}
		
		 	//level = new Level();
		 	//level.New(63, 63);
			//_worldStage.addActor(level);
			setGameState(GameState.PlayMode);
	}
	public void Save() {		
		Json json = new Json();
		// TODO Auto-generated method stub
		FileHandle handle = Gdx.files.local("save.txt");
		//OutputStream sr = handle.write(false);
		//if(map != null)
		//map.Save(sr);
		save = json.prettyPrint(level);
		Logger.Log("Saved!");
		handle.writeString(save, false);
		
		
	}


	public void Load() {
		Json json = new Json();
		if(save == null){
			FileHandle handle = Gdx.files.local("save.txt");
			save = handle.readString();
		}
		// TODO Auto-generated method stub
		if(level != null)
		{
			level.remove();
			level = null;
		}
			level = json.fromJson(Level.class, save);
			Logger.Log("Loaded!");
			//_worldStage.addActor(level);
			setGameState(GameState.PlayMode);
		//map.Load();
	}


	public GameState getGameState() {
		return _gameState;
	}


	public void setGameState(GameState _gameState) {
		//Logic could go here to rebuild navmesh etc.
		this._gameState = _gameState;
	}


	public boolean checkMapCollision(int x, int y) {
		Vector2 vec = _worldStage.screenToStageCoordinates(new Vector2(x*64, y*64));
		GridActor hitActor = (GridActor) _worldStage.hit(vec.x, vec.y, true);
		if(hitActor != null && hitActor.get_owner()instanceof GameObject){
			return true;
		}
		return false;
	}


	public int getWidth() {
		return level.Width;
	}


	public int getHeight() {
		return level.Height;
	}

	public Room _currentRoom;
	
	public void BeginRoomBuild(Room dataObject) {
		_currentRoom = dataObject;
		
	}
}
