/**
 * 
 */
package com.mythiksoftware.hosp;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

/**
 * @author James
 *
 */
public class Map {
private TiledMap map;
private OrthogonalTiledMapRenderer renderer;
private OrthographicCamera camera;
public Map(OrthographicCamera orthographicCamera){
	camera = orthographicCamera;
	map = new TmxMapLoader().load("data/map.tmx");
	renderer = new OrthogonalTiledMapRenderer(map, 1f);
	//camera = new OrthographicCamera();
	//camera.setToOrtho(false, 30, 20);
	//camera.update();
}

public void render(){
	renderer.setView(camera);
	renderer.render();


}

public Cell getCell(int x,int y){
	TiledMapTileLayer a = (TiledMapTileLayer) map.getLayers().getLayer(0);
	return a.getCell(x,y) ;
}
}