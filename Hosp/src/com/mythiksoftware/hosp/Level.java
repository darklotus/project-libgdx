/**
 * 
 */
package com.mythiksoftware.hosp;

import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;


import Helpers.Logger;
import Helpers.Logger.LogLevel;
import Objects.BaseObject;
import Objects.GameObject;
import Objects.GridActor;
import Objects.MapTile;
import Objects.Patient;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.badlogic.gdx.utils.OrderedMap;
import com.sun.org.apache.xml.internal.utils.ObjectStack;

/**
 * @author James
 *
 */
public class Level extends Group implements Json.Serializable{

public int Height;
public int Width;
private ArrayList<BaseObject> objects = new ArrayList<BaseObject>();
private final Vector2 point = new Vector2();

private int _spawnX = 0,_spawnY = 0;
public ArrayList<Patient> patients = new ArrayList<Patient>();
	public Level(){

		/*for(int x = 0;x < 32;x++){
			for(int y = 0;y < 32;y++){
				MapTile tile = new MapTile(x,y,0,4);
				//tile.setPosition(x*64, y*64);
				addActor(tile);
			}
		}*/
	}
	public void act (float delta) {
		super.act(delta);
		if(patients.size() < 2)
		{
			spawnPatient();
		}
	}
	
	private void spawnPatient() {
		Patient patient = new Patient("Sonny");
		patient.init();
		this.addActor(patient);
		this.patients.add(patient);
		Logger.Log("Patient Spawned!", LogLevel.Debug);
	}
	public Actor hit (float x, float y, boolean touchable) {
		if (touchable && getTouchable() == Touchable.disabled) return null;
		Array<Actor> children = this.getChildren();
		for (int i = children.size - 1; i >= 0; i--) {
			Actor child = children.get(i);
			if (!child.isVisible()) continue;
			child.parentToLocalCoordinates(point.set(x, y));
			Actor hit = child.hit(point.x, point.y, touchable);
			if (hit != null) return hit;
		}
		return super.hit(x, y, touchable);
	}
	
	private void load(String mapname){			
		Width = 63; Height = 63;
	FileHandle fileHandle = Gdx.files.internal("data/"+mapname+".json");
		
	}
	public void loadMap(String mapname){

			Width = 63; Height = 63;
			FileHandle fileHandle = Gdx.files.internal("data/"+mapname+".json");
			OrderedMap root = (OrderedMap) new JsonReader().parse(fileHandle);
			//height = Integer.parseInt(root.get("height")));
			//width = (Integer) root.get("width");
			
			boolean bTerrain = false;
			Array<OrderedMap> layers =  (Array<OrderedMap>) root.get("layers");
			for (int i = 0; i < layers.size; i++) {
				if(layers.get(i)== null)
					continue;
				if(layers.get(i).get("name").toString().contains("terrain"))
					bTerrain = true;
				else {
					bTerrain = false;
				}
				Array dataString = (Array) layers.get(i).get("data");
				int x = 0, y = 63;
				for (Object string : dataString) {
					float id = (Float) string;
					if(id > 1){
						Objects.BaseObject tile = null;
						if(bTerrain){
						tile = new Objects.MapTile(MathUtils.round(id));
						tile.setPosition(x,y);
						this.addActor(tile.getActor());
						}	
						else {
							tile = new Objects.GameObject(MathUtils.round(id));
							tile.setPosition(x,y);
							this.addActor(tile.getActor());

						}
					}
					x++;
					if(x == 64){
						x = 0; y--;
						if(y < 0)
							y++;
					}
				}
				
			}
	}

	

	

	public void New(int x, int y) {
		//todo load a blank world just grass?
		loadMap("map");
		
	}

	public void addActor(BaseObject _currentBuildObject) {
		super.addActor(_currentBuildObject.getActor());
		objects.add(_currentBuildObject);
	}

	@Override
	public void write(Json json) {
		int x =0;

		for (int i = 0; i < getChildren().size; i++) {
			GridActor actor = (GridActor) getChildren().get(i);
			//if(actor.get_owner() instanceof GameObject){
			json.writeValue(x+"",actor.get_owner());x++;
			
			//}
		}
		Logger.Log("Saved " + x + " items");
	}

	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData) {
		Logger.Log("load sees " + jsonData.size);
		for (Iterator iterator = jsonData.entries(); iterator.hasNext();) {
			Entry type = (Entry) iterator.next();
			BaseObject object = BaseObject.deserialize(json,(OrderedMap<String, Object>) type.value);
			this.addActor(object.getActor());
			Logger.Log("loaded item id " + (String)type.key);
		}
		

		
	}

	
}
