package Events;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener.FocusEvent.Type;

abstract public class UIListener implements EventListener {
	public boolean handle (Event event) {
		if (!(event instanceof UIEvent)) return false;
		changed((UIEvent)event, event.getTarget());
		return false;
	}

	/** @param actor The event target, which is the actor that emitted the change event. */
	abstract public void changed (UIEvent event, Actor actor);

	/** Fired when something in an actor has changed. This is a generic event, exactly what changed in an actor will vary.
	 * @author Nathan Sweet */
	static public class UIEvent extends Event {
		private UIEventEnum type;
		private Object dataObject;
		public void reset () {
			super.reset();
			setType(null);
			setDataObject(null);
		}

		public UIEventEnum getType() {
			return type;
		}

		public void setType(UIEventEnum type) {
			this.type = type;
		}

		public Object getDataObject() {
			return dataObject;
		}

		public void setDataObject(Object dataObject) {
			this.dataObject = dataObject;
		}
	}
	public enum UIEventEnum {
		BuildModeEnabled,
		BuildModeDisabled,
		BuildObject,
		BuildRoom,
		RoomModeEnabled,
		RoomModeDisabled,
		Save,
		Load, NewGame
		
	}
}