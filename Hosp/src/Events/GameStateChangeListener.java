package Events;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener.FocusEvent.Type;

abstract public class GameStateChangeListener implements EventListener {
	public boolean handle (Event event) {
		if (!(event instanceof GameStateChangeEvent)) return false;
		StateChanged((GameStateChangeEvent)event, event.getTarget());
		return false;
	}

	/** @param actor The event target, which is the actor that emitted the change event. */
	abstract public void StateChanged (GameStateChangeEvent event, Actor actor);

	/** Fired when something in an actor has changed. This is a generic event, exactly what changed in an actor will vary.
	 * @author Nathan Sweet */
	static public class GameStateChangeEvent extends Event {
		private String type;

		public void reset () {
			super.reset();
			setType(null);
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}
}