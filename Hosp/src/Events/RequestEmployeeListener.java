package Events;

import Groups.Room;
import Helpers.Employees;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener.FocusEvent.Type;
/** Fired when a room needs an employee, ONLY fire from rooms.
 * @author James Kidd */
abstract public class RequestEmployeeListener implements EventListener {
	public boolean handle (Event event) {
		if (!(event instanceof RequestEmployeeEvent)) return false;
		EmployeeRequest((RequestEmployeeEvent)event, (Room) event.getTarget());
		return false;
	}

	/** @param actor The event target, which is the actor that emitted the change event. */
	abstract public void EmployeeRequest (RequestEmployeeEvent event, Room actor);

	/** Fired when a room needs an employee
	 * @author James Kidd */
	static public class RequestEmployeeEvent extends Event {
		private Employees type;

		public void reset () {
			super.reset();
			setType(null);
		}

		public Employees getType() {
			return type;
		}

		public void setType(Employees type) {
			this.type = type;
		}
	}
}