/**
 * 
 */
package AI;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

import Groups.Room;
import Helpers.Logger;
import Objects.Doctor;
import Objects.NPC;
import Objects.Patient;

/**
 * @author James
 *
 */
public class DoctorBrain extends Action {

	private Doctor _owner;


	public DoctorBrain(Doctor owner)
	{
		_owner = owner;
	}
	int destx = 0;

	
	@Override
	public boolean act(float delta) {
		if(destx == 0){
			destx = _owner.getX() + 10;
			
			Logger.Log("Did move action in new patient");
		}
		return false;
	}

}
