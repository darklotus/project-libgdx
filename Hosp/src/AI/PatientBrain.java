/**
 * 
 */
package AI;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;


import Groups.Room;
import Helpers.Logger;
import Objects.NPC;
import Objects.Patient;

/**
 * @author James
 *
 */
public class PatientBrain extends Action {

	private Patient _owner;


	public PatientBrain(Patient owner)
	{
		_owner = owner;
	}
	int destx = 0;

	
	@Override
	public boolean act(float delta) {
		if(destx == 0){
			destx = _owner.getX() + 10;
			_owner.getActor().addAction(Actions.moveBy(10*64, 0, 5));
			
			Logger.Log("Did move action in new patient");
		}
		return false;
	}

}
