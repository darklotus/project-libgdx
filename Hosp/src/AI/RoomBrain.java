/**
 * 
 */
package AI;

import Events.RequestEmployeeListener.RequestEmployeeEvent;
import Events.UIListener.UIEvent;
import Events.UIListener.UIEventEnum;
import Groups.Room;
import Helpers.Employees;
import Objects.NPC;
import Objects.Patient;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Pools;

/**
 * @author James
 *
 */
public class RoomBrain extends Action {

	private Room _owner;
	private Patient _currentPatient;
	private Action _currentAction;
	
	public RoomBrain(Room owner)
	{
		_owner = owner;
	}
	/* (non-Javadoc)
	 * @see com.badlogic.gdx.scenes.scene2d.Action#act(float)
	 */
	@Override
	public boolean act(float delta) {
		if(_currentAction != null || _owner == null)
			return false;
		if(_owner.getCurrentEmployee() == null)
			requestDoctor();
		if(_currentPatient == null)
			_currentPatient = requestPatient();
		//At this point handle doctor/patient entering doing stuff as a state machine using smaller individual actions.
		return false;
	}
	private Patient requestPatient() {		
		Patient patient = _owner.getNextPatient();
		return patient;

	}
	private void requestDoctor() {
		RequestEmployeeEvent uievent = Pools.obtain(RequestEmployeeEvent.class);
		uievent.setType(Employees.Doctor);
		_owner.fire(uievent);
		Pools.free(uievent);	
	}

}
