package Helpers;

public enum Employees {
	Doctor,
	Nurse,
	Receptionist,
	Surgeon,
	Pathologist,
	Diagnostician,
	Psychiatrist,
	Cleaner
}
