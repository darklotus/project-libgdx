/**
 * 
 */
package Objects;

import Helpers.Logger;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.badlogic.gdx.utils.OrderedMap;
import com.mythiksoftware.hosp.Level;

/**
 * @author James
 *
 */
public class BaseObject implements Json.Serializable {
	
	
	
	protected GridActor _actor;
	protected int x,y;
	protected int _id;
	protected String _nameString;
	public BaseObject(int id){
		_id = id;
		init();

	}
	public BaseObject(String name){
		_nameString = name;
		init();

	}
	public BaseObject(){

	}
	
	public void write (Json json) {
		if(_nameString != null)
	      json.writeValue("name", _nameString);
		else {
			json.writeValue("id", Integer.toString(_id));
		}
		json.writeValue("x", Integer.toString(x));
		json.writeValue("y", Integer.toString(y));
	   }


	   public static BaseObject deserialize(Json json,OrderedMap<String, Object> value) {
			BaseObject o = new BaseObject();
			o.read(json, value);
			return o;
		}
	  
	   
	public void init(){
		if(_nameString == null)
			_actor = new GridActor(this, _id);
		else
			_actor = new GridActor(this, _nameString);
		setX(x);
		setY(y);
	}
	   
	public void setX(int x) {
		this.x = x;
		_actor.setX(x*64);
	}
	public int getY() {
		return y;
	}
	public int getX()
	{
		return x;
	}
	public void setY(int y) {
		this.y = y;
		_actor.setY(y*64);
	}
	public void setPosition(int x2, int y2) {
		//Logger.Log("setting pos" + x2 + " " + y2);
		setX(x2);
		setY(y2);
		
	}
	public void setColor(Color color) {
		_actor.setColor(color);
		
	}
	
	public void setTouchability(Touchable touchable)
	{
		this._actor.setTouchable(touchable);
	}

	public Actor getActor() {
		return _actor;
	}
	public void read(Json json, OrderedMap<String, Object> jsonData) {
	      Entry<String, Object> entry = jsonData.entries().next();
	     if(entry.key.contains("name"))
	    	 _nameString = (String) entry.value;
	     else
	    	 _id = Integer.parseInt((String) entry.value);

	     x = Integer.parseInt((String) jsonData.get("x", 0));

	     y = Integer.parseInt((String) jsonData.get("y", 0));
	     init();
		
	}
	
	
	
	
	
}
