/**
 * 
 */
package Objects;


import AI.DoctorBrain;
import AI.PatientBrain;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;

/**
 * @author James
 *
 */
public class Doctor extends GameObject {

	private DoctorBrain AI;

	public Doctor(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.utils.Json.Serializable#read(com.badlogic.gdx.utils.Json, com.badlogic.gdx.utils.OrderedMap)
	 */
	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void init(){
		super.init();
		this.AI = new DoctorBrain(this);
	}
	

}
