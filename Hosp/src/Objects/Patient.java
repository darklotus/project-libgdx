/**
 * 
 */
package Objects;


import AI.PatientBrain;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;

/**
 * @author James
 *
 */
public class Patient extends BaseObject {

	

	public Patient(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.utils.Json.Serializable#read(com.badlogic.gdx.utils.Json, com.badlogic.gdx.utils.OrderedMap)
	 */
	@Override
	public void read(Json json, OrderedMap<String, Object> jsonData) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void init(){
		super.init();
		getActor().addAction(new PatientBrain(this));
	}
	

}
