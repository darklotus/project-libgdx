/**
 * 
 */
package Objects;


import java.io.Console;

import Events.UIListener;
import Events.UIListener.UIEvent;
import Helpers.GraphicsManager;
import Helpers.Logger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mythiksoftware.hosp.Screens.InGameScreen;

/**
 * @author James
 *
 */
public class GridActor extends Actor {
	TextureRegion region;

	private BaseObject _owner;
	
	/*public GridActor(String name) {
		 region = GraphicsManager.getManager().ObjectNames.get(name);
		 setWidth(region.getRegionWidth());
		 setHeight(region.getRegionHeight());
        //setWidth(64);
        //setHeight(64);
        //TODO fix widthheight
       
	}*/
	public GridActor(BaseObject owner,String name) {
		 region = GraphicsManager.getManager().ObjectNames.get(name);
		 setWidth(region.getRegionWidth());
		 setHeight(region.getRegionHeight());
         set_owner(owner);
         //TODO fix widthheight
        
	}
	public GridActor(BaseObject owner,int id) {
   	 region = GraphicsManager.getManager().getTileWithID(id);
   	 setWidth(region.getRegionWidth());
	 setHeight(region.getRegionHeight());
        set_owner(owner);
	}
	public void draw (SpriteBatch batch, float parentAlpha) {
        Color oldColor = batch.getColor();    
		Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(region, getX(), getY(), getOriginX(), getOriginY(),
        		getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        batch.setColor(oldColor);
        
        
}
	/*public void setPosition(float x,float y)
	{
		setX(Math.round(x/64)*64);
		setY(Math.round(y/64)*64);
	}*/
	public BaseObject get_owner() {
		return _owner;
	}
	public void set_owner(BaseObject _owner) {
		this._owner = _owner;
	}

}
