/**
 * 
 */
package UI;

/**
 * @author James
 *
 */
public interface MythikUI {

	public void render(float delta);
	public void resize(int width,int height);
	public void dispose();
}
