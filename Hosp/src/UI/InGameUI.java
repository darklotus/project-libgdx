/**
 * 
 */
package UI;


import java.util.Iterator;

import Events.UIListener.UIEvent;
import Events.UIListener.UIEventEnum;
import Groups.GPOffice;
import Groups.Room;
import Helpers.GraphicsManager;
import Helpers.Logger;
import Objects.GameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.mythiksoftware.hosp.HospitalMain;
import com.mythiksoftware.hosp.Screens.InGameScreen;
import com.mythiksoftware.hosp.Screens.MainMenuScreen;

/**
 * @author James
 *
 */
public class InGameUI  extends Actor implements MythikUI {
public enum MenuType {
	MainMenu,
	ObjectPlacmentMenu,
	RoomSelectionMenu,
	RoomBuildMenu
}
	
	private Stage _stage;

	private Skin _skin;
	
	private Table _mainMenuTable;
	private Table _objectPlacementMenu;
	private Table _roomSelectionMenu;
	private Table _roomBuildMenu;

	public InGameUI()  {
		_stage = new Stage();//(0, 0, true);
		_skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		_skin.getAtlas().getTextures().iterator().next().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		Gdx.input.setInputProcessor(_stage);
		setupMainMenu();
		setupObjectPlacementTable();
		setupRoomSelectionMenu();
		Show(MenuType.MainMenu);
	}
	private void setupRoomSelectionMenu() {
		_roomSelectionMenu = new Table();
		_roomSelectionMenu.setFillParent(true);
		_roomSelectionMenu.right();
		_roomSelectionMenu.top();
		
		TextButton button = new TextButton("GP Office", _skin);
		button.addListener(new ChangeListener() {			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				setupRoomBuildMenu(new GPOffice());
				if(_roomBuildMenu.getStage() == null)
					Show(MenuType.RoomBuildMenu);	
				UIEvent uievent = Pools.obtain(UIEvent.class);
				uievent.setType(UIEventEnum.BuildRoom);
				uievent.setDataObject(new GPOffice());
				fire(uievent);
				Pools.free(uievent);
			}

			
		});
		_roomSelectionMenu.add(button);
		
		
	}
	
	private void setupRoomBuildMenu(Room gpOffice) {
		_roomBuildMenu = new Table();
		_roomBuildMenu.setFillParent(true);
		_roomBuildMenu.right();
		_roomBuildMenu.top();
		
		for (final String string : gpOffice.neededObjects) {
			TextureRegion img = (TextureRegion) GraphicsManager.getManager().ObjectNames.get(string);
			Image a = new Image(img);
			ImageButton b = new ImageButton(a.getDrawable());
			b.addListener(new ChangeListener() {
				
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					GameObject Gobject = new GameObject(string);
					UIEvent uievent = Pools.obtain(UIEvent.class);
					uievent.setType(UIEventEnum.BuildObject);
					uievent.setDataObject(Gobject);
					fire(uievent);
					Pools.free(uievent);
					
				}
			});
			_roomBuildMenu.add(b);
		}
		
	}
	private void setupObjectPlacementTable() {
		_objectPlacementMenu = new Table();
		_objectPlacementMenu.setFillParent(true);
		int cnt = 0;
		Table table2 = new Table();
		for (Iterator i = GraphicsManager.getManager().ObjectNames.keySet().iterator(); i.hasNext();) {
			final String string = (String) i.next();
			TextureRegion img = (TextureRegion) GraphicsManager.getManager().ObjectNames.get(string);
			Image a = new Image(img);
			final ImageButton b = new ImageButton(a.getDrawable());
			b.addListener(new ChangeListener() {
				
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					GameObject Gobject = new GameObject(string);
					UIEvent uievent = Pools.obtain(UIEvent.class);
					uievent.setType(UIEventEnum.BuildObject);
					uievent.setDataObject(Gobject);
					fire(uievent);
					Pools.free(uievent);
					b.remove();
				}
			});
			table2.add(b);
			cnt++;
			if(cnt > 3){
				table2.row();
				cnt = 0;}
		}
		//_BuildMenuTable.setScale(0.5f);
		ScrollPane pane = new ScrollPane(table2,_skin);
		pane.setCancelTouchFocus(false);
		_objectPlacementMenu.add(pane).maxWidth(300);//.expand().fill().colspan(3);	
		_objectPlacementMenu.right();
		_objectPlacementMenu.top();

	}
	private void setupMainMenu() {
		_mainMenuTable = new Table();
		_mainMenuTable.setFillParent(true);		
		TextButton button = new TextButton("BuildMode", _skin);
		button.addListener(new ChangeListener() {			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(_objectPlacementMenu.getStage() == null)
					Show(MenuType.ObjectPlacmentMenu);	
				else
					Hide(MenuType.ObjectPlacmentMenu);
			}
		});
		_mainMenuTable.add(button);
		TextButton button2 = new TextButton("Save Game", _skin);
		button2.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				UIEvent uievent = Pools.obtain(UIEvent.class);
				uievent.setType(UIEventEnum.Save);
				fire(uievent);
				Pools.free(uievent);
				
			}
		});
		_mainMenuTable.add(button2);
		
		TextButton button11 = new TextButton("Load Game", _skin);
		button11.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				UIEvent uievent = Pools.obtain(UIEvent.class);
				uievent.setType(UIEventEnum.Load);
				fire(uievent);
				Pools.free(uievent);
				
			}
		});
		_mainMenuTable.add(button11);
		TextButton buttonNewGame = new TextButton("New Game", _skin);
		buttonNewGame.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				UIEvent uievent = Pools.obtain(UIEvent.class);
				uievent.setType(UIEventEnum.NewGame);
				fire(uievent);
				Pools.free(uievent);
				
			}
		});
		_mainMenuTable.add(buttonNewGame);
		
		
		TextButton button3 = new TextButton("Build Room", _skin);
		button3.addListener(new ChangeListener() {			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(_roomSelectionMenu.getStage() == null)
					Show(MenuType.RoomSelectionMenu);	
				else
					Hide(MenuType.RoomSelectionMenu);
			}
		});
		_mainMenuTable.add(button3);
		_mainMenuTable.left();
		_mainMenuTable.top();
	}
	public void Show(MenuType type){
		switch(type){
		case MainMenu:
			_stage.addActor(_mainMenuTable);
			break;
		case ObjectPlacmentMenu:
			_stage.addActor(_objectPlacementMenu);
			break;
		case RoomSelectionMenu:
			_stage.addActor(_roomSelectionMenu);
			break;
		case RoomBuildMenu:
			_stage.addActor(_roomBuildMenu);
			break;
			
		}
		Logger.Log("Showing : " + type.toString());
		
	}
	public void Hide(MenuType type){
		switch(type){
		case MainMenu:
			_mainMenuTable.remove();
			break;
		case ObjectPlacmentMenu:
			_objectPlacementMenu.remove();
			break;
		case RoomSelectionMenu:
			_roomSelectionMenu.remove();
			break;
		case RoomBuildMenu:
			_roomBuildMenu.remove();
			break;
		}
		Logger.Log("Hiding : " + type.toString());
	}

	@Override
	public void render(float delta) {
		//_mainMenuTable.debugTable(); // turn on only table lines
		//_objectPlacementTable.debugTable(); // turn on only table lines
		 _stage.act(delta);
		 _stage.draw();
	    // Table.drawDebug(_stage); // This is optional, but enables debug lines for tables.
		
	}

	@Override
	public void resize(int width, int height) {
		_stage.setViewport(width, height, true);
		//_table.setPosition((_stage.getWidth() / 2 - 100), (int)(_stage.getHeight() / 2.2));
	}

	@Override
	public void dispose() {
		_stage.dispose();
		
	}
	
	
	
	
}
