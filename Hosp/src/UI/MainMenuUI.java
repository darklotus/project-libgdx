/**
 * 
 */
package UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.mythiksoftware.hosp.HospitalMain;
import com.mythiksoftware.hosp.Screens.InGameScreen;

/**
 * @author James
 *
 */
public class MainMenuUI implements MythikUI {
private Stage _stage;
	private Table _table;
	private Skin _skin;
	public MainMenuUI(){
		_stage = new Stage();//(0, 0, true);
		_skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		_skin.getAtlas().getTextures().iterator().next().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		Gdx.input.setInputProcessor(_stage);
		_table = new Table();
		_stage.addActor(_table);
		setupUI();
	}
	private void setupUI() {
		Label label = new Label("Welcome",_skin);
		_table.add(label);
		_table.row();
		TextButton button = new TextButton("Start New Game", _skin);
		button.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				HospitalMain.StaticRefrence.setScreen(new InGameScreen());
				
			}
		});
		_table.add(button);
		_table.row();
		_table.row();
		TextButton load = new TextButton("Load Saved Game", _skin);
		load.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				HospitalMain.StaticRefrence.setScreen(new InGameScreen(true));
				
			}
		});
		_table.add(load);
		_table.setSize(200, 300);
		_table.setPosition((_stage.getWidth() / 2 - 100), (_stage.getHeight() / 2 - 100));
		//_table.pack();
		
		
	}
	@Override
	public void render(float delta) {
		 Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		 _stage.act(Gdx.graphics.getDeltaTime());
		 _stage.draw();

	       // Table.drawDebug(_stage); // This is optional, but enables debug lines for tables.
		
	}

	@Override
	public void resize(int width, int height) {
		_stage.setViewport(width, height, true);
		_table.setPosition((_stage.getWidth() / 2 - 100), (int)(_stage.getHeight() / 2.2));
	}

	@Override
	public void dispose() {
		_stage.dispose();
		
	}

}
