/**
 * 
 */
package Actions;

import Helpers.Logger;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

/**
 * @author James
 *
 */
public class UseObject extends TemporalAction {

	

	
	
	protected void begin () {
		Logger.Log("Begin in UseObject");
		setDuration(3);
	}

	protected void update (float percent) {
		actor.setScale(percent);
	}

	
}
