/**
 * 
 */
package Actions;

import Helpers.Logger;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author James
 *
 */
public class PathFindAction extends Action {

	@Override
	public boolean act(float delta) {
		if((int)getActor().getX() == endX || (int)getActor().getY() == endY)
		{
			return true;
		}
		return false;
	}
	
	
	private int startX, startY;
	private int endX, endY;
	
	@Override
	public void setActor (Actor actor) {
		super.setActor(actor);		
		Logger.Log("Begin in pather");
		startX = (int) actor.getX()/64;
		startY = (int) actor.getY()/64;
	}
	
	

	public void setPosition (float x, float y) {
		endX = (int) (x*64);
		endY = (int) (y*64);
	}

	public float getX () {
		return endX;
	}


	public float getY () {
		return endY;
	}

	

}
